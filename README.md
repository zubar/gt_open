# Технологии и возможности#
*FatFreeFramework, jQuery, Без использование орм, Имеется аудит действий пользователей (отключено, делалось для последующей синхронизации между устройствами для клиентских приложений), шаблонизатор не используется*

# Установка #

1. Создайте БД и залейте дамп 
2. Измените параметры сайта

## Дамп стартовой БД в файле dump.sql ##

## Параметры сайта в файле app/config.ini ##

```
#!php

; Mysql DSN
ENCODING="UTF8"
db.dsn="mysql:dbname=<название базы данных>;host=127.0.0.1;"
db.user="<имя пользователя БД>"
db.password="<пароль пользователя БД>"
db.prefix="gt_"
; Site properties
site.url="<адрес сайта, пример: http://gt_open>"
site.name="GROUP-TASKS"
site.company="GROUP-TASKS.COM"
site.support.email="support@gt_open"
site.support.author="Техническая поддержка"
; Email settings
smtp.host = smtp.yandex.ru
smtp.debug = 0
smtp.auth = true
smtp.port = 465
smtp.secure ="ssl"
smtp.username = "support@gt_open"
smtp.password = "<пароль к почте>"
smtp.addreply = "support@gt_open"
smtp.replyto ="support@gt_open"
```

### Тестовый пользователь ###

email=test@site15.ru
password=test