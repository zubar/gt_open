<?php

class Access extends \Prefab {

    function getAll($project_id) {
        $user_id = $this->app->user->getData('user.id');
        $access = new MyMapper('{prefix}projects_access');
        $access->user_name = 'u.username';
        $access->user_email = 'u.email';
        $rows = $access->find(array("{prefix}projects_access t, {prefix}users u "
            . "where u.id=t.user_id and t.user_id<>? and "
            . "t.project_id=? and "
            . "t.project_id in (select pa.project_id from {prefix}projects_access pa where pa.user_id=?)", $user_id, $project_id, $user_id), array('query' => true));
        if ($rows != false)
            return $rows;
        else
            return array();
    }

    function get($project_id, $id) {
        $access = new MyMapper('{prefix}projects_access');
        $row = $access->load(array("project_id=? and id=?", $project_id, $id));
        if ($row !== false) {
            return $access;
        }
        return false;
    }

    function getByUserId($project_id, $user_id) {
        $access = new MyMapper('{prefix}projects_access');
        $row = $access->load(array("project_id=? and user_id=?", $project_id, $user_id));
        if ($row !== false) {
            return $access;
        }
        return false;
    }

    function create($project_id, $params) {
        $project = Projects::instance()->get($project_id);
        if ($project !== false) {
            $access = new MyMapper('{prefix}projects_access');
            $access->id = Utils::instance()->guid();
            if (!isset($params['user_id'])) {
                if (isset($params['email'])) {
                    $user = Users::instance()->get($params['email']);
                    if ($user == false) {
                        $user = Users::instance()->create(array('email' => $params['email'], 'username' => $params['email'], 'password' => Utils::instance()->guid()));
                        $reg_link = $this->app->get('site.url') . '/activate?key=' . $user->code;
                    }
                }
            } else {
                $user = Users::instance()->getById($params['user_id']);
            }
            $access->user_id = $user->id;
            $access->project_id = $project_id;
            $exists = $this->getByUserId($access->project_id, $access->user_id);
            if ($exists === false) {
                if (isset($params['access']))
                    $access->access = $params['access'];
                $row = $access->insert();
                if ($row !== false) {
                    MyAudit::instance()->create($project_id, 'projects_access', $access->id, $access->cast(), 'create');
                    if (isset($reg_link))
                        Messages::instance()->sent('sentregivitekey', $user->email, array('user' => $user->cast(), 'project' => $project->cast(), 'link' => $this->app->get('site.url') . '/project/' . $project_id, 'reg_link' => $reg_link));
                    else {
                        if ($user->key != '')
                            Messages::instance()->sent('sentivitekey', $user->email, array('user' => $user->cast(), 'project' => $project->cast(), 'link' => $this->app->get('site.url') . '/project/' . $project_id));
                    }
                    return $access;
                }
            } else
                return $exists;
        }
        return false;
    }

    function update($project_id, $id, $params) {
        $project = Projects::instance()->get($project_id);
        if ($project !== false) {
            $access = $this->get($project_id, $id);
            if ($access !== false && (intval($access->user_id) != intval($project->user_id) || intval($project->user_id) == intval($this->app->user->getData('user.id')))) {
                $access_data = $access->cast();
                if (isset($params['access'])) {
                    if ($access->access != $params['access'] && $params['access'] == 1) {
                        $user = $this->app->user->getById($access->user_id);
                        if ($user->key != '')
                            Messages::instance()->sent('sentivitekey', $user->email, array('user' => $user->cast(), 'project' => $project->cast(), 'link' => $this->app->get('site.url') . '/project/' . $project_id));
                    }
                    $access->access = $params['access'];
                }
                $audit = MyAudit::instance()->create($project_id, 'projects_access', $access->id, $access->cast(), 'update', $access_data);
                $row = $access->update();
                if ($row !== false) {
                    return $access;
                }
                $audit->erase();
            }
        }
        return false;
    }

    function delete($project_id, $id) {
        $project = Projects::instance()->get($project_id);
        if ($project !== false) {
            $access = $this->get($project_id, $id);
            if ($access !== false && (intval($access->user_id) != intval($project->user_id) || intval($project->user_id) == intval($this->app->user->getData('user.id')))) {
                MyAudit::instance()->create($project_id, 'projects_access', $access->id, $access->cast(), 'delete');
                $result = $access->erase();
                if ($result !== false) {
                    return true;
                }
            }
        }
        return false;
    }

    function __construct() {
        $this->app = Base::instance();
    }

}
