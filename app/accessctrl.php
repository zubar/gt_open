<?php

class AccessCtrl extends Controller {

    function getAllForm($app, $args) {
        if (isset($args['id'])) {
            $project = Projects::instance()->get($args['id']);
            if ($project) {
                $app->set('accessList', Access::instance()->getAll($project->id));
                $app->set('title', 'Доступ');
                $app->set('content', 'access/listPage.htm');
                $app->set('contentClass', 'access-list');
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }

    function createForm($app, $args) {
        if (isset($args['id'])) {
            $project = Projects::instance()->get($args['id']);
            $app->set('title', 'Добавить пользователя');
            $app->set('content', 'access/createForm.htm');
            $app->set('contentClass', 'access-create');
        } else
            $app->error(404);
    }

    function createAction($app, $args) {
        $access = Access::instance()->create($args['id'], $app->get('POST'));
        if ($access === false) {
            $app->error(403);
        } else {
            $app->status(200);
        }
    }

    function deleteAction($app, $args) {
        if (isset($args['id']) && isset($args['accessId'])) {
            $access = Access::instance()->delete($args['id'], $args['accessId']);
            if ($access !== false) {
                $app->status(200);
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }

    function updateAction($app, $args) {
        if (isset($args['id']) && isset($args['accessId'])) {
            $access = Access::instance()->update($args['id'], $args['accessId'], $app->get('POST'));
            if ($access === false) {
                $app->error(403);
            } else {
                $app->status(200);
            }
        } else {
            $app->error(404);
        }
    }

}
