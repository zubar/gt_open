<?php

class AppCtrl extends Controller {

    /**
     * 
     * @param Base $app
     */
    function startPage($app) {
        if ($app->user->getData('role') === 'user') {
            $app->reroute('/projects');
        } else {
            $app->set('title', 'Главная');
            $app->set('contentClass', 'padingtop15');
            $app->set('content', 'home/startPage.htm');
        }
    }

    /**
     * 
     * @param Base $app
     */
    function contactPage($app) {
        if ($app->user->getData('role') === 'user') {
            $app->set('user', $app->user->getData('user'));
        }
        $app->set('title', 'Написать письмо админу');
        $app->set('content', 'home/contactPage.htm');
    }

    /**
     * 
     * @param Base $app
     */
    function contactAction($app) {
        if ($app->exists('POST.email') && $app->exists('POST.username') && $app->exists('POST.message')) {
            $result = Messages::instance()->sent('contactmessage', $app->get('site.support.email'), array(
                'user'=>array('username'=>$app->get('site.support.author'),'email'=>$app->get('site.support.email')),
                'message' => $app->get('POST.message'),
                'username' => $app->get('POST.username'),
                'email' => $app->get('POST.email')));
            if ($result !== false) {
                $app->status(200);
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }

    /**
     * 
     * @param Base $app
     */
    function donatePage($app) {
        $app->set('title', 'Помощь сервису');
        $app->set('contentClass', 'padingtop15');
        $app->set('content', 'home/donatePage.htm');
    }
    
    /**
     * 
     * @param Base $app
     */
    function thanksPage($app) {
        $app->set('title', 'Помощь сервису');
        $app->set('contentClass', 'padingtop15');
        $app->set('content', 'home/thanksPage.htm');
    }

    /**
     * 
     * @param Base $app
     */
    function errorPage($app) {
        if ($app->get('ERROR.text') != '') {
            $log = new Log('error.log');
            $log->write($app->get('ERROR.text'));
            if (is_array($app->get('ERROR.trace'))) {
                foreach ($app->get('ERROR.trace') as $frame)
                    if (isset($frame['file'])) {
                        // Parse each backtrace stack frame
                        $line = '';
                        $addr = $app->fixslashes($frame['file']) . ':' . $frame['line'];
                        if (isset($frame['class']))
                            $line.=$frame['class'] . $frame['type'];
                        if (isset($frame['function'])) {
                            $line.=$frame['function'];
                            if (!preg_match('/{.+}/', $frame['function'])) {
                                $line.='(';
                                if (isset($frame['args']) && $frame['args'])
                                    $line.=$app->csv($frame['args']);
                                $line.=')';
                            }
                        }
                        // Write to custom log
                        $log->write($addr . ' ' . $line);
                    }
            }
        }
        $app->set('title', 'Ошибка');
        $app->set('content', 'home/errorPage.htm');
    }

}
