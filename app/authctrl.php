<?php

class AuthCtrl extends Controller {

    function profileForm($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        $app->set('user', $app->user->getData('user'));
        $app->set('title', 'Редактирование профиля');
        $app->set('content', 'auth/profileForm.htm');
        $app->set('contentClass', 'profile-update');
    }

    function profileAction($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        $user = $app->user->update($app->user->getData('user.id'), $app->get('POST'));
        if ($user === false) {
            $app->error(403);
        } else {
            $app->status(200);
        }
    }

    function changepassForm($app) {
        $app->set('title', 'Восстановление доступа');
        if ($app->exists('GET.key'))
            $app->set('activateKey', $app->get('GET.key'));
        else
            $app->set('activateKey', '');
        $app->set('content', 'auth/changePassForm.htm');
    }

    function changepassAction($app) {
        if ($app->get('POST.key') != '' && $app->get('POST.password') != '') {
            $user = $app->user->getByKey($app->get('POST.key'));
            if ($user === false) {
                $app->error(404);
            } else {
                $user = $app->user->changePass($user, $app->get('POST.password'));
                $app->user->setUser($user);
                $app->status(200);
            }
        } else
            $app->error(400);
    }

    function recoveryForm($app) {
        $app->set('title', 'Восстановление доступа');
        $app->set('content', 'auth/recoveryForm.htm');
    }

    function recoveryAction($app) {
        if ($app->get('POST.email') != '') {
            $user = $app->user->getByEmail($app->get('POST.email'));
            if ($user === false) {
                $app->error(404);
            } else {
                $user = $app->user->setNewKey($user);
                Messages::instance()->sent('sentrecoverykey', $user->email, array('user' => $user->cast(), 'link' => $app->get('site.url') . '/changepass?key=' . $user->code));
                $app->status(200);
            }
        } else
            $app->error(400);
    }

    function loginForm($app) {
        $app->set('title', 'Авторизация');
        $app->set('content', 'auth/loginForm.htm');
    }

    function loginAction($app) {
        if ($app->get('POST.email') != '' && $app->get('POST.password') != '') {
            $user = $app->user->get($app->get('POST.email'), $app->get('POST.password'));
            if ($user === false) {
                $app->error(404);
            } else {
                if ($user->code == '') {
                    $app->user->setUser($user);
                    $app->status(200);
                } else
                    $app->error(403);
            }
        } else
            $app->error(400);
    }

    function logoutAction($app) {
        $app->user->clearUser();
        $app->status(200);
    }

    function regForm($app) {
        $app->set('title', 'Регистрация');
        $app->set('content', 'auth/regForm.htm');
    }

    function regAction($app) {
        if ($app->get('POST.email') != '' && $app->get('POST.username') != '' && $app->get('POST.password') != '') {
            if (!$app->user->getByEmail($app->get('POST.email'))) {
                $user = $app->user->create($app->get('POST'));
                if ($user === false) {
                    $app->error(403);
                } else {
                    Messages::instance()->sent('sentregistrationkey', $user->email, array('user' => $user->cast(), 'link' => $this->app->get('site.url') . '/activate?key=' . $user->code));
                    $app->status(200);
                }
            } else
                $app->error(409);
        } else
            $app->error(400);
    }

    function activateRegKeyForm($app, $args) {
        $app->set('title', 'Активация учетной записи');
        if ($app->exists('GET.key'))
            $app->set('activateKey', $app->get('GET.key'));
        else
            $app->set('activateKey', '');
        $app->set('content', 'auth/activateRegKeyForm.htm');
    }

    function activateRegKeyAction($app) {
        if ($app->get('POST.key') != '') {
            $user = $app->user->getByKey($app->get('POST.key'));
            if ($user !== false) {
                $app->user->setUser($user);
                $app->user->clearKey($user);
                $app->status(200);
            } else
                $app->error(400);
        } else
            $app->error(400);
    }

}
