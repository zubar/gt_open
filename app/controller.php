<?php

//! Base controller
class Controller {

    protected $app;

    //! Instantiate class
    function __construct() {
        $this->app = Base::instance();
    }

    //! HTTP route pre-processor
    function beforeroute($app) {
        $app->user = Users::instance();
        if ($app->exists('POST.localtime')) {
            $app->user->setData('localtime', intval($app->get('POST.localtime')));
            $difference = Utils::instance()->now() - intval($app->get('POST.localtime'));
            if ($difference <= 30)
                $difference = 0;
            $app->user->setData('difference', $difference);
        }
        if (isset($_SERVER['HTTP_REFERER']))
            $app->set('referer', $_SERVER['HTTP_REFERER']);
        else
            $app->set('referer', '');
        if ($app->user->getData('role') !== 'user') {
            $app->set('header', 'guest/headerBlock.htm');
            $app->set('footer', 'guest/footerBlock.htm');
        } else {
            if ($app->user->getData('user.admin') == '1') {
                $app->set('header', 'admin/headerBlock.htm');
                $app->set('footer', 'admin/footerBlock.htm');
            } else {
                $app->set('projectsList', Projects::instance()->getAll());
                $app->set('header', 'user/headerBlock.htm');
                $app->set('footer', 'user/footerBlock.htm');
            }
        }
    }

    //! HTTP route post-processor
    function afterroute($app) {
        if ($app->exists('out') && $app->get('out') != '') {
            $out = $app->get('out');
            if (is_array($out) || is_object($out)) {
                header("Content-Type: application/json", true);
                echo json_encode($out);
            } else {
                echo $out;
            }
        } else
        if ($app->exists('content') && $app->get('content') != '') {
            if ($app->get('AJAX'))
                echo View::instance()->render(F3::get('content'));
            else
                echo View::instance()->render('layout.htm');
        }
    }

}
