<?php

class Messages extends \Prefab {

    private $app;

    function sent($template, $email, $params = array()) {
        try {
            $this->app->set('params', $params);
            if (!class_exists('PHPMailer')) {
                require 'PHPMailer/PHPMailerAutoload.php';
            }
//Create a new PHPMailer instance
            $mail = new PHPMailer();

// Set PHPMailer to use the sendmail transport
            $mail->setLanguage($this->app->get('userlang'));
            $mail->isSMTP();

            $mail->Host = $this->app->get('smtp.host');
            $mail->SMTPDebug = $this->app->get('smtp.debug');
            $mail->SMTPAuth = $this->app->get('smtp.auth');
            $mail->SMTPSecure = $this->app->get('smtp.secure');
            $mail->Port = $this->app->get('smtp.port');
            $mail->Username = $this->app->get('smtp.username');
            $mail->Password = $this->app->get('smtp.password');
            
            $mail->CharSet = 'UTF-8';
//Set who the message is to be sent from
            $mail->setFrom($this->app->get('site.support.email'), $this->app->get('site.company'));
//Set who the message is to be sent to
            $mail->addAddress($email, $email);
//Set the subject line
            $mail->Subject = htmlspecialchars($this->app->get('site.company') . ': ' . $this->app->get('message.' . $template . '.them'));
//Read an HTML message body from an external file, convert referenced images to embedded,
            $this->app->set('template_subject', $mail->Subject);

//convert HTML into a basic plain-text alternative body
            $this->app->set('template_html_header', '../messages/' . $this->langDir . 'header/html.tpl');
            $this->app->set('template_html_content', '../messages/' . $this->langDir . $template . '/html.tpl');
            $this->app->set('template_html_footer', '../messages/' . $this->langDir . 'footer/html.tpl');
            $mail->msgHTML(Template::instance()->render('../messages/html.tpl'), dirname(__FILE__) . '../message/' . $template);
//Replace the plain text body with one created manually
            $this->app->set('template_text_header', '../messages/' . $this->langDir . 'header/text.tpl');
            $this->app->set('template_text_content', '../messages/' . $this->langDir . $template . '/text.tpl');
            $this->app->set('template_text_footer', '../messages/' . $this->langDir . 'footer/text.tpl');
            $mail->AltBody = Template::instance()->render('../messages/text.tpl');
//send the message, check for errors
            if (!$mail->send()) {
                Utils::instance()->toLog($mail->ErrorInfo);
                Utils::instance()->toLog('Message:' . $mail->AltBody);
                return $mail->ErrorInfo;
            } else
                return true;
        } catch (phpmailerException $e) {
            //Utils::instance()->toLog($e->errorMessage());
        } catch (Exception $e) {
            //Utils::instance()->toLog($e->getMessage());
        }
    }

    function __construct() {
        $this->app = Base::instance();
        $this->langDir = $this->app->get('langDir');
    }

}
