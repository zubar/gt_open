<?php

class MyAudit extends \Prefab {

    function create($project_id, $table, $rowid, $data, $action, $dataBeforeUpdate = null) {
        $audit = new MyMapper('{prefix}audit');
        $audit->id = Utils::instance()->guid();
        $audit->project_id = $project_id;
        $audit->modifi_table = $table;
        $audit->modifi_rowid = $rowid;
        $audit->modifi_action = $action;
        $audit->insert_date = Utils::instance()->now();
        $audit->user_id = $this->app->user->getData('user.id');

        if ($action == 'update') {
            if ($dataBeforeUpdate === null) {
                $obj = new MyMapper($table);
                if ($obj->load(array('id=?', $rowid))) {
                    $obj_data = $obj->cast();
                    $new_data = array();
                    foreach ($obj_data as $key => $value) {
                        if (isset($data[$key]) && $data[$key] != $value) {
                            $new_data[$key] = $data[$key];
                        }
                    }
                    $data = $new_data;
                }
            } else {
                $new_data = array();
                foreach ($dataBeforeUpdate as $key => $value) {
                    if (isset($data[$key]) && $data[$key] != $value) {
                        $new_data[$key] = $data[$key];
                    }
                }
                $data = $new_data;
            }
        }
        $audit->modifi_rowdata = json_encode($data);
        $row = $audit->insert();
        if ($row !== false) {
            return $audit;
        }
        return false;
    }

    function __construct() {
        $this->app = Base::instance();
    }

}
