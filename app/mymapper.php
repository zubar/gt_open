<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MyMapper extends DB\SQL\Mapper {

    public function __construct($table, $fields = NULL, $ttl = 60) {
        $table = str_replace('{prefix}', \Base::instance()->get('db.prefix'), $table);
        parent::__construct(Base::instance()->db, $table, $fields, $ttl);
    }

    static function exec($cmds, $args = NULL, $ttl = 0, $log = TRUE) {
        if (is_string($cmds))
            $cmds = str_replace('{prefix}', F3::get('db.prefix'), $cmds);
        if (is_array($cmds) && isset($cmds[0]) && is_string($cmds[0]))
            $cmds[0] = str_replace('{prefix}', F3::get('db.prefix'), $cmds[0]);
        return Base::instance()->db->exec($cmds, $args, $ttl, $log);
    }

    /**
     * 	Return records that match criteria
     * 	@return array
     * 	@param $filter string|array
     * 	@param $options array
     * 	@param $ttl int
     * */
    function find($filter = NULL, array $options = NULL, $ttl = 0) {
        if (!$options)
            $options = array();
        $options+=array(
            'group' => NULL,
            'order' => NULL,
            'limit' => 0,
            'offset' => 0
        );
        if (isset($options['query'])) {
            $adhoc = '';
            foreach ($this->adhoc as $key => $field)
                $adhoc.=',' . $field['expr'] . ' AS ' . $this->db->quotekey($key);
            return $this->select('t.'.implode(',t.', array_map(array($this->db, 'quotekey'), array_keys($this->fields))) .
                            $adhoc, $filter, $options, $ttl);
        } else {
            $adhoc = '';
            foreach ($this->adhoc as $key => $field)
                $adhoc.=',' . $field['expr'] . ' AS ' . $this->db->quotekey($key);
            return $this->select(($options['group']? : implode(',', array_map(array($this->db, 'quotekey'), array_keys($this->fields)))) .
                            $adhoc, $filter, $options, $ttl);
        }
    }

    /**
     * 	Build query string and execute
     * 	@return array
     * 	@param $fields string
     * 	@param $filter string|array
     * 	@param $options array
     * 	@param $ttl int
     * */
    function select($fields, $filter = NULL, array $options = NULL, $ttl = 0) {
        if (!$options)
            $options = array();
        $options+=array(
            'group' => NULL,
            'order' => NULL,
            'limit' => 0,
            'offset' => 0
        );
        if (isset($options['query'])) {
            if ($filter) {
                if (is_array($filter)) {
                    $args = isset($filter[1]) && is_array($filter[1]) ?
                            $filter[1] :
                            array_slice($filter, 1, NULL, TRUE);
                    $args = is_array($args) ? $args : array(1 => $args);
                    list($filter) = $filter;
                }
                $filter = str_replace('{prefix}', \Base::instance()->get('db.prefix'), $filter);
            }
            else
            {
                $filter=$this->table.' t';
            }
            $sql = 'SELECT ' . $fields . ' FROM ' . $filter;
            $sql = str_replace('{prefix}', \Base::instance()->get('db.prefix'), $sql);
        } else {
            $sql = 'SELECT ' . $fields . ' FROM ' . $this->table;
            $args = array();
            if ($filter) {
                if (is_array($filter)) {
                    $args = isset($filter[1]) && is_array($filter[1]) ?
                            $filter[1] :
                            array_slice($filter, 1, NULL, TRUE);
                    $args = is_array($args) ? $args : array(1 => $args);
                    list($filter) = $filter;
                }
                $filter = str_replace('{prefix}', \Base::instance()->get('db.prefix'), $filter);
                $sql.=' WHERE ' . $filter;
            }
            $db = $this->db;
            if ($options['group'])
                $sql.=' GROUP BY ' . implode(',', array_map(
                                        function($str) use($db) {
                                    return preg_match('/^(\w+)(?:\h+HAVING|\h*(?:,|$))/i', $str, $parts) ?
                                            ($db->quotekey($parts[1]) .
                                            (isset($parts[2]) ? (' ' . $parts[2]) : '')) : $str;
                                }, explode(',', $options['group'])));
            if ($options['order']) {
                $sql.=' ORDER BY ' . implode(',', array_map(
                                        function($str) use($db) {
                                    return preg_match('/^(\w+)(?:\h+(ASC|DESC))?\h*(?:,|$)/i', $str, $parts) ?
                                            ($db->quotekey($parts[1]) .
                                            (isset($parts[2]) ? (' ' . $parts[2]) : '')) : $str;
                                }, explode(',', $options['order'])));
            }
            if ($options['limit'])
                $sql.=' LIMIT ' . (int) $options['limit'];
            if ($options['offset'])
                $sql.=' OFFSET ' . (int) $options['offset'];
        }
        $result = $this->db->exec($sql, $args, $ttl);
        $out = array();
        foreach ($result as &$row) {
            foreach ($row as $field => &$val) {
                if (array_key_exists($field, $this->fields)) {
                    if (!is_null($val) || !$this->fields[$field]['nullable'])
                        $val = $this->db->value(
                                $this->fields[$field]['pdo_type'], $val);
                }
                elseif (array_key_exists($field, $this->adhoc))
                    $this->adhoc[$field]['value'] = $val;
                unset($val);
            }
            $out[] = $this->factory($row);
            unset($row);
        }
        return $out;
    }

}
