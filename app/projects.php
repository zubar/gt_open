<?php

class Projects extends \Prefab {

    function getAll() {
        $projects = new MyMapper('{prefix}projects');
        $projects->tasks_completed = 'SUM(IFNULL(tsk.completed,0))';
        $projects->tasks_count = 'COUNT(tsk.id)';
        $projects->access_on = '(select count(1) from {prefix}projects_access a where a.access=1 and t.id=a.project_id AND a.user_id<>t.user_id)';
        $projects->access_count = '(select count(1) from {prefix}projects_access a where t.id=a.project_id AND a.user_id<>t.user_id)';
        $rows = $projects->find(array("{prefix}projects t "
            . "LEFT JOIN {prefix}tasks tsk ON t.id=tsk.project_id "
            . "where t.id in (select pa.project_id from "
            . "{prefix}projects_access pa where pa.user_id=?)"
            . "GROUP BY t.id,t.title,t.description,t.icon,t.completed_date,t.user_id,t.insert_date, t.pos "
            . "ORDER by pos",
            $this->app->user->getData('user.id')), array('query' => true));
        if ($rows != false)
            return $rows;
        else
            return array();
    }

    function maxPos() {
        $project = new MyMapper('{prefix}projects');
        $project->maxPos = 'MAX(pos)';
        $row = $project->load();
        if ($row !== false) {
            return intval($project->maxPos);
        }
        return 0;
    }

    function set($data) {

        $data['accessStr'] = '';
        if (intval($data['access_on']) > 0 || intval($data['access_count']) > 0)
            $data['accessStr'] = $data['access_on'] . '/' . $data['access_count'];

        $data['completedStr'] = '';
        if (intval($data['tasks_completed']) > 0 || intval($data['tasks_count']) > 0)
            $data['completedStr'] = $data['tasks_completed'] . '/' . $data['tasks_count'];

        if (intval($data['completed_date']) > 30)
            $data['completedDate'] = intval($data['completed_date']) - Users::instance()->getData('difference', 0);
        else
            $data['completedDate'] = 0;

        if ($data['icon'] == '')
            $data['icon'] = 'glyphicon glyphicon-flag';

        $data['title'] = stripslashes($data['title']);
        $data['description'] = stripslashes($data['description']);
        $this->app->set('projectItem', $data);
    }

    function get($id) {
        $project = new MyMapper('{prefix}projects');
        $project->tasks_completed = 'SUM(IFNULL(tsk.completed,0))';
        $project->tasks_count = 'COUNT(tsk.id)';
        $project->access_on = '(select count(1) from {prefix}projects_access a where a.access=1 and t.id=a.project_id AND a.user_id<>t.user_id)';
        $project->access_count = '(select count(1) from {prefix}projects_access a where t.id=a.project_id AND a.user_id<>t.user_id)';
        $row = $project->load(array("{prefix}projects t "
            . "LEFT JOIN {prefix}tasks tsk ON t.id=tsk.project_id "
            . "where t.id in (select pa.project_id from "
            . "{prefix}projects_access pa where pa.user_id=?) and "
            . "t.id=?"
            . "GROUP BY t.id,t.title,t.description,t.icon,t.completed_date,t.user_id,t.insert_date, t.pos ",
            $this->app->user->getData('user.id'), $id), array('query' => true));
        if ($row !== false) {
            $this->set($project->cast());
            return $project;
        }
        return false;
    }

    function create($params) {
        $project = new MyMapper('{prefix}projects');
        $project->id = Utils::instance()->guid();
        if (isset($params['title']))
            $project->title = $params['title'];
        if (isset($params['icon']))
            $project->icon = $params['icon'];
        if (isset($params['description']))
            $project->description = $params['description'];
        if (isset($params['pos']))
            $project->pos = intval($params['pos']);
        else
            $project->pos = $this->maxPos() + 1;
        if (isset($params['completed_date'])) {
            if (intval($params['completed_date']) > 30)
                $project->completed_date = intval($params['completed_date']) + $this->app->user->getData('difference');
            else
                $project->completed_date = 0;
        }
        $project->insert_date = Utils::instance()->now();
        $project->user_id = $this->app->user->getData('user.id');
        $err = array();
        if ($project->title == '')
            $err[] = 'Название обязательно для заполнения';
        if (count($err) == 0) {
            $row = $project->insert();
            if ($row !== false) {
                MyAudit::instance()->create($project->id, 'projects', $project->id, $project->cast(), 'create');
                $project_access = new MyMapper('{prefix}projects_access');
                $project_access->project_id = $project->id;
                $project_access->user_id = $this->app->user->getData('user.id');
                $project_access->insert_date = Utils::instance()->now();
                $row = $project_access->insert();
                if ($row !== false) {
                    MyAudit::instance()->create($project->id, 'projects_access', $project_access->id, $project_access->cast(), 'create');
                    return $project;
                }
            }
        }
        return false;
    }

    function copy($id, $params) {
        $real_project = $this->get($id);
        if ($real_project !== false) {
            if ($params['title'] == $real_project->title) {
                $params['title'] = $params['title'] . ' copy';
            }
            if (intval($params['completed_date']) > 0) {
                $params['completed_date'] = Utils::instance()->now() + intval($params['completed_date']) - intval($real_project->insert_date);
            }
            $project = $this->create($params);
            if ($project !== false) {
                $tasks = Tasks::instance()->getAll($id);
                foreach ($tasks as $task) {
                    $task->completed = 0;
                    if (intval($task->completed_date) > 0) {
                        $task->completed_date = Utils::instance()->now() + intval($task->completed_date) - intval($task->insert_date);
                    }
                    Tasks::instance()->create($project->id, $task->cast());
                }
                $access = Access::instance()->getAll($id);
                foreach ($access as $accessItem) {
                    Access::instance()->create($project->id, $accessItem);
                }
                return $project;
            }
        }
        return false;
    }

    function change($from, $to) {
        $rows = $this->getAll();
        $poss = array();
        if (count($rows) > 0 && $from < count($rows) && $to < count($rows)) {
            for ($index = min(array($from, $to)); $index <= max(array($from, $to)); $index++) {
                $poss[$index] = $rows[$index]->pos;
            }
            if ($from < $to) {
                for ($index = $from + 1; $index <= $to; $index++) {
                    $rows[$index]->pos = $poss[$index - 1];
                    $rows[$index]->update();
                }
                $rows[$from]->pos = $poss[$to];
                $rows[$from]->update();
                return true;
            } else
            if ($from > $to) {
                for ($index = $to; $index < $from; $index++) {
                    $rows[$index]->pos = $poss[$index + 1];
                    $rows[$index]->update();
                }
                $rows[$from]->pos = $poss[$to];
                $rows[$from]->update();
                return true;
            }
        }
        return false;
    }

    function update($id, $params) {
        $project = $this->get($id);
        if ($project !== false) {
            $project_data = $project->cast();
            if (isset($params['title']))
                $project->title = $params['title'];
            if (isset($params['icon']))
                $project->icon = $params['icon'];
            if (isset($params['description']))
                $project->description = $params['description'];
            if (isset($params['pos']))
                $project->pos = intval($params['pos']);
            if (isset($params['completed_date'])) {
                if (intval($params['completed_date']) > 30)
                    $project->completed_date = intval($params['completed_date']) + $this->app->user->getData('difference');
                else
                    $project->completed_date = 0;
            }
            $err = array();
            if ($project->title == '')
                $err[] = 'Название обязательно для заполнения';
            if (count($err) == 0) {
                $audit = MyAudit::instance()->create($project->id, 'projects', $project->id, $project->cast(), 'update', $project_data);
                $row = $project->update();
                if ($row !== false) {
                    return $project;
                }
                $audit->erase();
            }
        }
        return false;
    }

    function delete($id) {
        $project = $this->get($id);
        if ($project !== false && $project->user_id == $this->app->user->getData('user.id')) {
            MyAudit::instance()->create($project->id, 'projects', $project->id, $project->cast(), 'delete');
            $result = $project->erase();
            if ($result !== false) {
                return $project;
            }
        }
        return false;
    }

    function __construct() {
        $this->app = Base::instance();
    }

}
