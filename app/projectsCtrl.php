<?php

class ProjectsCtrl extends Controller {

    function getAllForm($app) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        $app->set('title', 'Проекты');
        $app->set('content', 'projects/listPage.htm');
        $app->set('contentClass', 'projects-list');
    }

    function createForm($app) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        $app->set('title', 'Новый проект');
        $app->set('content', 'projects/createForm.htm');
        $app->set('contentClass', 'project-create');
    }

    function createAction($app) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        $project = Projects::instance()->create($app->get('POST'));
        if ($project === false) {
            $app->error(403);
        } else {
            $app->status(200);
        }
    }

    function updateForm($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id'])) {
            $project = Projects::instance()->get($args['id']);
            if ($project !== false) {
                $app->set('title', 'Изменение проекта');
                Projects::instance()->set($project->cast());
                $app->set('content', 'projects/updateForm.htm');
                $app->set('contentClass', 'project-update');
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }

    function changeAction($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if ($app->exists('POST.from')&&$app->exists('POST.to')) {
            $result = Projects::instance()->change(intval($app->get('POST.from')),intval($app->get('POST.to')));
            if ($result !== false) {
                $app->status(200);
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }
    
    function deleteAction($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id'])) {
            $project = Projects::instance()->delete($args['id']);
            if ($project !== false) {
                $app->status(200);
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }
    
    function copyAction($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id'])) {
            $item = Projects::instance()->copy($args['id'], $app->get('POST'));
            if ($item === false) {
                $app->error(403);
            } else {
                $app->set('out', array('project_id'=>$item->id));
                $app->status(200);
            }
        } else {
            $app->error(404);
        }
    }

    function updateAction($app, $args) {
        if ($app->exists('POST.act')&&$app->get('POST.act')=='copy')
        {
            $this->copyAction($app, $args);
            return;
        }
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id'])) {
            $project = Projects::instance()->update($args['id'], $app->get('POST'));
            if ($project === false) {
                $app->error(403);
            } else {
                $app->status(200);
            }
        } else {
            $app->error(404);
        }
    }

}
