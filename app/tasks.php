<?php

class Tasks extends \Prefab {

    function getAll($project_id) {
        $tasks = new MyMapper('{prefix}tasks');
        $rows = $tasks->find(array("project_id=? and project_id in (select pa.project_id from {prefix}projects_access pa where pa.user_id=?)", $project_id, $this->app->user->getData('user.id')), array('order' => 'pos'));
        if ($rows != false)
            return $rows;
        else
            return array();
    }

    function maxPos($project_id) {
        $task = new MyMapper('{prefix}tasks');
        $task->maxPos = 'MAX(pos)';
        $row = $task->load(array("project_id=?", $project_id));
        if ($row !== false) {
            return intval($task->maxPos);
        }
        return 0;
    }

    function set($data) {

        if (intval($data['completed_date']) > 30)
            $data['completedDate'] = intval($data['completed_date']) - Users::instance()->getData('difference', 0);
        else
            $data['completedDate'] = 0;

        $data['title'] = stripslashes($data['title']);
        $data['description'] = stripslashes($data['description']);
        if (intval($data['completed']) == 1) {
            $data['checked'] = 'checked';
        } else {
            $data['checked'] = '';
            $data['completedDate'] = 0;
        }

        $this->app->set('taskItem', $data);
    }

    function get($project_id, $id) {
        $task = new MyMapper('{prefix}tasks');
        $row = $task->load(array("project_id=? and id=?", $project_id, $id));
        if ($row !== false) {
            $this->set($task->cast());
            return $task;
        }
        return false;
    }

    function create($project_id, $params) {
        $task = new MyMapper('{prefix}tasks');
        $task->id = Utils::instance()->guid();
        $task->project_id = $project_id;
        if (isset($params['title']))
            $task->title = $params['title'];
        if (isset($params['description']))
            $task->description = $params['description'];
        if (isset($params['pos']))
            $task->pos = $params['pos'];
        else
            $task->pos = $this->maxPos($project_id) + 1;
        if (isset($params['completed_date'])) {
            if (intval($params['completed_date']) > 30)
                $task->completed_date = intval($params['completed_date']) + $this->app->user->getData('difference');
            else
                $task->completed_date = 0;
        }

        if (isset($params['completed'])) {
            $task->completed = $params['completed'];
            if ($task->completed == 1) {
                $task->completed_date = Utils::instance()->now();
                $task->completed_user_id = $this->app->user->getData('user.id');
            }
        }
        if (isset($params['repeat_interval']))
            $task->repeat_interval = $params['repeat_interval'];
        $task->insert_date = Utils::instance()->now();
        $task->user_id = $this->app->user->getData('user.id');
        $err = array();
        if ($task->title == '')
            $err[] = 'Название обязательно для заполнения';
        if (count($err) == 0) {
            $row = $task->insert();
            if ($row !== false) {
                MyAudit::instance()->create($project_id, 'tasks', $task->id, $task->cast(), 'create');
                return $task;
            }
        }
        return false;
    }

    function copy($project_id, $id, $params) {
        $real_task = $this->get($project_id, $id);
        if ($real_task !== false) {
            if ($params['title'] == $real_task->title) {
                $params['title'] = $params['title'] . ' copy';
            }
            $params['completed'] = 0;
            if (intval($params['completed_date']) > 0) {
                $params['completed_date'] = Utils::instance()->now() + intval($params['completed_date']) - intval($real_task->insert_date);
            }
            $task = $this->create($project_id, $params);
            return $task;
        }
        return false;
    }

    function change($project_id, $from, $to) {
        $rows = $this->getAll($project_id);
        $poss = array();
        if (count($rows) > 0 && $from < count($rows) && $to < count($rows)) {
            for ($index = min(array($from, $to)); $index <= max(array($from, $to)); $index++) {
                $poss[$index] = $rows[$index]->pos;
            }
            if ($from < $to) {
                for ($index = $from + 1; $index <= $to; $index++) {
                    $rows[$index]->pos = $poss[$index - 1];
                    $rows[$index]->update();
                }
                $rows[$from]->pos = $poss[$to];
                $rows[$from]->update();
                return true;
            } else
            if ($from > $to) {
                for ($index = $to; $index < $from; $index++) {
                    $rows[$index]->pos = $poss[$index + 1];
                    $rows[$index]->update();
                }
                $rows[$from]->pos = $poss[$to];
                $rows[$from]->update();
                return true;
            }
        }
        return false;
    }

    function update($project_id, $id, $params) {
        $task = $this->get($project_id, $id);
        if ($task !== false) {
            $task_data = $task->cast();
            if (isset($params['title']))
                $task->title = $params['title'];
            if (isset($params['description']))
                $task->description = $params['description'];
            if (isset($params['pos']))
                $task->pos = $params['pos'];
            if (isset($params['completed_date'])) {
                if (intval($params['completed_date']) > 30)
                    $task->completed_date = intval($params['completed_date']) + $this->app->user->getData('difference');
                else
                    $task->completed_date = 0;
            }
            if (isset($params['completed'])) {
                $task->completed = $params['completed'];
                if ($task->completed == 1) {
                    $task->completed_date = Utils::instance()->now();
                    $task->completed_user_id = $this->app->user->getData('user.id');
                }
            }
            if (isset($params['repeat_interval']))
                $task->repeat_interval = $params['repeat_interval'];
            $err = array();
            if ($task->title == '')
                $err[] = 'Название обязательно для заполнения';
            if (count($err) == 0) {
                $audit = MyAudit::instance()->create($project_id, 'tasks', $task->id, $task->cast(), 'update', $task_data);
                $row = $task->update();
                if ($row !== false) {
                    return $task;
                }
                $audit->erase();
            }
        }
        return false;
    }

    function delete($project_id, $id) {
        $task = $this->get($project_id, $id);
        if ($task !== false) {
            MyAudit::instance()->create($project_id, 'tasks', $task->id, $task->cast(), 'delete');
            $result = $task->erase();
            if ($result !== false) {
                return true;
            }
        }
        return false;
    }

    function __construct() {
        $this->app = Base::instance();
    }

}
