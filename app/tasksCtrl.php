<?php

class TasksCtrl extends Controller {

    function getAllForm($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id'])) {
            $project = Projects::instance()->get($args['id']);
            if ($project) {
                $app->set('tasksList', Tasks::instance()->getAll($project->id));
                $app->set('title', 'Задачи');
                $app->set('content', 'tasks/listPage.htm');
                $app->set('contentClass', 'tasks-list');
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }

    function createForm($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id'])) {
            $project = Projects::instance()->get($args['id']);
            $app->set('title', 'Новая задача');
            $app->set('content', 'tasks/createForm.htm');
            $app->set('contentClass', 'task-create');
        } else
            $app->error(404);
    }

    function createAction($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        $task = Tasks::instance()->create($args['id'], $app->get('POST'));
        if ($task === false) {
            $app->error(403);
        } else {
            $app->status(200);
        }
    }

    function updateForm($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id']) && isset($args['taskId'])) {
            $project = Projects::instance()->get($args['id']);
            $task = Tasks::instance()->get($args['id'], $args['taskId']);
            if ($project !== false) {
                if ($task !== false) {
                    $app->set('title', 'Изменение задачи');
                    $app->set('content', 'tasks/updateForm.htm');
                    $app->set('contentClass', 'task-update');
                } else {
                    $app->error(404);
                }
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }

    function changeAction($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id']) && $app->exists('POST.from') && $app->exists('POST.to')) {
            $result = Tasks::instance()->change($args['id'], intval($app->get('POST.from')), intval($app->get('POST.to')));
            if ($result !== false) {
                $app->status(200);
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }

    function deleteAction($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id']) && isset($args['taskId'])) {
            $task = Tasks::instance()->delete($args['id'], $args['taskId']);
            if ($task !== false) {
                $app->status(200);
            } else {
                $app->error(404);
            }
        } else {
            $app->error(404);
        }
    }


    function copyAction($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id']) && isset($args['taskId'])) {
            $item = Tasks::instance()->copy($args['id'], $args['taskId'], $app->get('POST'));
            if ($item === false) {
                $app->error(403);
            } else {
                $app->set('out', array('task_id'=>$item->id));
                $app->status(200);
            }
        } else {
            $app->error(404);
        }
    }
    
    function updateAction($app, $args) {
        if ($app->exists('POST.act')&&$app->get('POST.act')=='copy')
        {
            $this->copyAction($app, $args);
            return;
        }
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id']) && isset($args['taskId'])) {
            $task = Tasks::instance()->update($args['id'], $args['taskId'], $app->get('POST'));
            if ($task === false) {
                $app->error(403);
            } else {
                $app->status(200);
            }
        } else {
            $app->error(404);
        }
    }

    function getForm($app, $args) {
        if ($app->user->getData('role') !== 'user') {
            $app->error(404);
            return;
        }
        if (isset($args['id']) && isset($args['taskId'])) {
            $task = Tasks::instance()->get($args['id'], $args['taskId']);
            if ($task !== false) {
                $app->set('title', 'Задача');
                $app->set('content', 'tasks/itemPage.htm');
                $app->set('contentClass', 'tasks-list');
            }
        } else {
            $app->error(404);
        }
    }

}
