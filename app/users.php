<?php

class Users extends \Prefab {

    /**
     *
     * @var Base 
     */
    private $app;

    function clearUser() {
        $this->setData('role', null);
        $this->setData('user', null);
    }

    function setUser($user) {
        $this->setData('role', 'user');
        $this->setData('user', $user->cast());
    }

    function changePass($user, $password) {
        $user->password = md5($password);
        $user->code = '';
        $user->update();
        return $user;
    }

    function setNewKey($user) {
        $user->code = Utils::instance()->guid();
        $user->update();
        return $user;
    }

    function clearKey($user) {
        $user->code = '';
        $user->update();
        return $user;
    }

    function get($email, $password = null) {
        if ($password === null) {
            $user = new MyMapper('{prefix}users');
            $row = $user->load(array("email=?", $email));
            if ($row !== false) {
                return $user;
            }
        } else {
            $user = new MyMapper('{prefix}users');
            $row = $user->load(array("email=? and password=?", $email, md5($password)));
            if ($row !== false) {
                return $user;
            }
        }
        return false;
    }

    function getByKey($key) {
        $user = new MyMapper('{prefix}users');
        $row = $user->load(array("code=?", $key));
        if ($row !== false) {
            return $user;
        }
        return false;
    }

    function getById($id) {
        $user = new MyMapper('{prefix}users');
        $row = $user->load(array("id=?", $id));
        if ($row !== false) {
            return $user;
        }
        return false;
    }

    function getByEmail($email) {
        $user = new MyMapper('{prefix}users');
        $row = $user->load(array("email=?", $email));
        if ($row !== false) {
            return $user;
        }
        return false;
    }

    function create($params) {
        if ($this->get($params['email']) === false) {
            $email = $params['email'];
            $username = $params['username'];
            $password = $params['password'];
            $user = new MyMapper('{prefix}users');
            $user->email = $email;
            $user->username = $username;
            $user->password = md5($password);
            $user->code = Utils::instance()->guid();
            $row = $user->insert();
            if ($row !== false) {
                return $user;
            }
        }
        return false;
    }

    function update($id, $params) {
        $user = $this->getById($id);
        if ($user !== false) {
            if (isset($params['email']))
                $user->email = $params['email'];
            if (isset($params['username']))
                $user->username = $params['username'];
            if (isset($params['password']) && $params['password'] != '')
                $user->password = md5($params['password']);
            $row = $user->update();
            if ($row !== false) {
                $this->setUser($user);
                return $user;
            }
        }
        return false;
    }

    function getData($key, $defValue = null) {
        if ($this->app->exists('SESSION.' . $key))
            return $this->app->get('SESSION.' . $key);
        else
            return $defValue;
    }

    function setData($key, $value = null) {
        $this->app->set('SESSION.' . $key, $value);
    }

    function __construct() {
        $this->app = Base::instance();
    }

}
