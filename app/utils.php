<?php

class Utils extends \Prefab {

    private $app;

    function GP($name, $emptyAsNull = false) {
        $result = null;

        if ($this->app->get('GET.' . $name) != null)
            $result = $this->app->get('GET.' . $name);

        if ($this->app->get('POST.' . $name) != null)
            $result = $this->app->get('POST.' . $name);

        if ($this->app->get('BODY') != '') {
            $payload = json_decode($this->app->get('BODY'));
            if (isset($payload->$name)) {
                $result = $payload->$name;
                if ($result === true)
                    $result = 1;
                if ($result === false)
                    $result = 0;
            }
        }

        if ($emptyAsNull && $result == false)
            return null;
        return $result;
    }

    function arrVal($array,$key,$defValue=null)
    {
        if (isset($array[$key]))
        {
            return $array[$key];
        }
        else
            return $defValue;
    }
    
    function guid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    function toLog($msg, $them = '') {
        if (intval($this->app->get('DEBUG')) > 0) {
            $log = new Log('debug.log');
            if (is_array($msg))
                $msg = print_r($msg,true);
            if (is_object($msg))
                $msg = print_r($msg,true);
            if ($them != '')
                $log->write($them);
            $log->write($msg);
        }
    }

    function now() {
        return round(microtime(true));// + 2 * 60 * 60;
    }

    function __construct() {
        $this->app = Base::instance();
        $this->app->set('myreplace', function($search, $replace, $subject) {
            return str_replace("[$search]", Base::instance()->get($replace), Base::instance()->get($subject));
        }
        );
    }

}
