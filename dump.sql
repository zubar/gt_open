-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.25 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица gt_open.gt_audit
CREATE TABLE IF NOT EXISTS `gt_audit` (
  `id` char(36) NOT NULL,
  `project_id` char(36) NOT NULL,
  `modifi_table` char(36) NOT NULL,
  `modifi_rowid` char(36) NOT NULL,
  `modifi_rowdata` text NOT NULL,
  `modifi_action` varchar(10) NOT NULL,
  `insert_date` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы gt_open.gt_audit: ~0 rows (приблизительно)
DELETE FROM `gt_audit`;
/*!40000 ALTER TABLE `gt_audit` DISABLE KEYS */;
INSERT INTO `gt_audit` (`id`, `project_id`, `modifi_table`, `modifi_rowid`, `modifi_rowdata`, `modifi_action`, `insert_date`, `user_id`) VALUES
	('20f29513-c23f-4c35-b0cd-d502d874ae32', '73a9e423-1cca-4477-b230-bb4774b66f21', 'tasks', 'cf5296ad-c260-43fe-81af-1ca4979451b0', '{"completed":1,"completed_date":1440739672,"completed_user_id":1}', 'update', 1440739672, 1),
	('56f8720b-1a27-4837-a747-2016c1d0600f', '73a9e423-1cca-4477-b230-bb4774b66f21', 'tasks', 'cf5296ad-c260-43fe-81af-1ca4979451b0', '{"id":"cf5296ad-c260-43fe-81af-1ca4979451b0","project_id":"73a9e423-1cca-4477-b230-bb4774b66f21","title":"task2","description":"task2","completed":0,"completed_date":0,"completed_user_id":0,"repeat_interval":0,"pos":2,"insert_date":1440739663,"user_id":1}', 'create', 1440739663, 1),
	('5b7d3bb2-0317-45ee-a243-1b204dba32e4', '73a9e423-1cca-4477-b230-bb4774b66f21', 'tasks', 'cf5296ad-c260-43fe-81af-1ca4979451b0', '{"completed":0}', 'update', 1440739674, 1),
	('78fb7a3e-0cd4-4598-9776-692e4dc15f13', '73a9e423-1cca-4477-b230-bb4774b66f21', 'tasks', 'cf5296ad-c260-43fe-81af-1ca4979451b0', '{"completed":1,"completed_date":1440739677}', 'update', 1440739677, 1),
	('e66b7c97-6bc0-4d95-bd35-6f45845f50ff', '73a9e423-1cca-4477-b230-bb4774b66f21', 'projects', '73a9e423-1cca-4477-b230-bb4774b66f21', '{"id":"73a9e423-1cca-4477-b230-bb4774b66f21","title":"test","description":"desc test","icon":"glyphicon glyphicon-thumbs-up brand-primary","pos":1,"completed_date":0,"user_id":1,"insert_date":1440739626}', 'create', 1440739626, 1),
	('e6ce150b-7b40-4556-8fc0-2bde31fa62ec', '73a9e423-1cca-4477-b230-bb4774b66f21', 'projects_access', '1', '{"id":1,"project_id":"73a9e423-1cca-4477-b230-bb4774b66f21","user_id":1,"access":1,"insert_date":"1440739626"}', 'create', 1440739627, 1),
	('e6e13e2a-df66-4bc1-b982-7c3fcb715aa6', '73a9e423-1cca-4477-b230-bb4774b66f21', 'tasks', 'b062f77b-c655-425a-86b0-4714fc4818d8', '{"id":"b062f77b-c655-425a-86b0-4714fc4818d8","project_id":"73a9e423-1cca-4477-b230-bb4774b66f21","title":"task1","description":"task1","completed":0,"completed_date":0,"completed_user_id":0,"repeat_interval":0,"pos":1,"insert_date":1440739653,"user_id":1}', 'create', 1440739653, 1);
/*!40000 ALTER TABLE `gt_audit` ENABLE KEYS */;


-- Дамп структуры для таблица gt_open.gt_projects
CREATE TABLE IF NOT EXISTS `gt_projects` (
  `id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `pos` int(11) NOT NULL,
  `completed_date` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `insert_date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gt_projects_gt_users` (`user_id`),
  CONSTRAINT `FK_gt_projects_gt_users` FOREIGN KEY (`user_id`) REFERENCES `gt_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы gt_open.gt_projects: ~0 rows (приблизительно)
DELETE FROM `gt_projects`;
/*!40000 ALTER TABLE `gt_projects` DISABLE KEYS */;
INSERT INTO `gt_projects` (`id`, `title`, `description`, `icon`, `pos`, `completed_date`, `user_id`, `insert_date`) VALUES
	('73a9e423-1cca-4477-b230-bb4774b66f21', 'test', 'desc test', 'glyphicon glyphicon-thumbs-up brand-primary', 1, 0, 1, 1440739626);
/*!40000 ALTER TABLE `gt_projects` ENABLE KEYS */;


-- Дамп структуры для таблица gt_open.gt_projects_access
CREATE TABLE IF NOT EXISTS `gt_projects_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(36) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_gt_projects_access_gt_projects` (`project_id`),
  KEY `FK_gt_projects_access_gt_users` (`user_id`),
  CONSTRAINT `FK_gt_projects_access_gt_projects` FOREIGN KEY (`project_id`) REFERENCES `gt_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_gt_projects_access_gt_users` FOREIGN KEY (`user_id`) REFERENCES `gt_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы gt_open.gt_projects_access: ~0 rows (приблизительно)
DELETE FROM `gt_projects_access`;
/*!40000 ALTER TABLE `gt_projects_access` DISABLE KEYS */;
INSERT INTO `gt_projects_access` (`id`, `project_id`, `user_id`, `access`) VALUES
	(1, '73a9e423-1cca-4477-b230-bb4774b66f21', 1, 1);
/*!40000 ALTER TABLE `gt_projects_access` ENABLE KEYS */;


-- Дамп структуры для таблица gt_open.gt_sessions
CREATE TABLE IF NOT EXISTS `gt_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '',
  `data` text,
  `csrf` text,
  `ip` varchar(40) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `stamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы gt_open.gt_sessions: ~2 rows (приблизительно)
DELETE FROM `gt_sessions`;
/*!40000 ALTER TABLE `gt_sessions` DISABLE KEYS */;
INSERT INTO `gt_sessions` (`session_id`, `data`, `csrf`, `ip`, `agent`, `stamp`) VALUES
	('kdeaqrvt850v3p0lunl7sof683', 'localtime|i:1440739592;difference|i:0;role|s:4:"user";user|a:5:{s:2:"id";i:1;s:8:"username";s:4:"user";s:8:"password";s:32:"ee11cbb19052e40b07aac0ca060c23ee";s:5:"email";s:14:"test@site15.ru";s:4:"code";s:0:"";}', '0b1u0ay1ixv.01ilkwwfolf', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1440739677),
	('s04go2d2rl2m303n32q00sq277', '', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1440735867);
/*!40000 ALTER TABLE `gt_sessions` ENABLE KEYS */;


-- Дамп структуры для таблица gt_open.gt_tasks
CREATE TABLE IF NOT EXISTS `gt_tasks` (
  `id` char(36) NOT NULL,
  `project_id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `completed` smallint(1) NOT NULL,
  `completed_date` int(11) NOT NULL,
  `completed_user_id` int(11) NOT NULL,
  `repeat_interval` int(11) NOT NULL,
  `pos` int(11) NOT NULL,
  `insert_date` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы gt_open.gt_tasks: ~0 rows (приблизительно)
DELETE FROM `gt_tasks`;
/*!40000 ALTER TABLE `gt_tasks` DISABLE KEYS */;
INSERT INTO `gt_tasks` (`id`, `project_id`, `title`, `description`, `completed`, `completed_date`, `completed_user_id`, `repeat_interval`, `pos`, `insert_date`, `user_id`) VALUES
	('b062f77b-c655-425a-86b0-4714fc4818d8', '73a9e423-1cca-4477-b230-bb4774b66f21', 'task1', 'task1', 0, 0, 0, 0, 1, 1440739653, 1),
	('cf5296ad-c260-43fe-81af-1ca4979451b0', '73a9e423-1cca-4477-b230-bb4774b66f21', 'task2', 'task2', 1, 1440739677, 1, 0, 2, 1440739663, 1);
/*!40000 ALTER TABLE `gt_tasks` ENABLE KEYS */;


-- Дамп структуры для таблица gt_open.gt_users
CREATE TABLE IF NOT EXISTS `gt_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `code` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы gt_open.gt_users: ~1 rows (приблизительно)
DELETE FROM `gt_users`;
/*!40000 ALTER TABLE `gt_users` DISABLE KEYS */;
INSERT INTO `gt_users` (`id`, `username`, `password`, `email`, `code`) VALUES
	(1, 'test', 'ee11cbb19052e40b07aac0ca060c23ee', 'test@site15.ru', '');
/*!40000 ALTER TABLE `gt_users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
