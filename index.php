<?php

header('Content-Type: text/html; charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
// Initialize APP
$app = require('lib/base.php');

if ((float) PCRE_VERSION < 7.9)
    trigger_error('PCRE version is out of date');

$app->config('app/config.ini');

// Define routes
$app->config('app/routes.ini');

// Connect to the database
$app->db = new DB\SQL($app->get('db.dsn'), $app->get('db.user'), $app->get('db.password'));

// Use database-managed sessions
new DB\SQL\Session($app->db, $app->get('db.prefix') . 'sessions');

$app->run();
