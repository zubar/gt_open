function resizeWindow() {
    if ($(window).width() <= 545)
    {
        $('.item-buttons,.item-buttons-link').removeClass('pull-left');

        $('.insert-button').removeClass('margin-top-15');
        $('.item-buttons').find('.btn').not('.btn-link').addClass('btn-block');
        $('.item-buttons-link').addClass('text-center');

        $('.media-body,.tasks-item,.access-item,.projects-item').find('.btn-group').addClass('btn-group-sm');
        $('body').addClass('padding-bottom-40 padding-top-40');
        $('.document-content').removeClass('container');
        $('.document-panel').removeClass('panel panel-default');
        $('.document-header').find('.navbar').addClass('navbar-fixed-top');
        $('.document-footer').find('.navbar').addClass('navbar-fixed-bottom');
    }
    else
    {
        $('.item-buttons,.item-buttons-link').addClass('pull-left');

        $('.insert-button').addClass('margin-top-15');
        $('.item-buttons').find('.btn').not('.btn-link').removeClass('btn-block');
        $('.item-buttons-link').removeClass('text-center');

        $('.media-body,.tasks-item,.access-item,.projects-item').find('.btn-group').removeClass('btn-group-sm');
        $('body').removeClass('padding-bottom-40 padding-top-40');
        $('.document-content').addClass('container');
        $('.document-panel').addClass('panel panel-default');
        $('.document-header').find('.navbar').removeClass('navbar-fixed-top');
        $('.document-footer').find('.navbar').removeClass('navbar-fixed-bottom');
    }
}
$(window).resize(function() {
    resizeWindow();
});
var redirectOn = true;
$(function() {
    resizeWindow();
    var now = moment();
    moment.locale('ru');
    $('.datetotimestamp').mask('00.00.0000 00:00');

    var draggableList = $('.draggableList');
    draggableList.sortable({
        placeholder: draggableList.data('placeholder'),
        handle: '.draggableItemButton',
        start: function(event, ui) {
            var start_pos = ui.item.index();
            ui.item.data('start_pos', start_pos);
        },
        update: function(event, ui) {
            var start_pos = ui.item.data('start_pos');
            var end_pos = ui.item.index();
            $.ajax({
                type: "POST",
                url: draggableList.data('url'),
                data: {from: start_pos, to: end_pos},
                error: function(responseText, statusText, xhr, $form) {
                    alert('Нет доступа');
                    return false;
                }
            });
        }
    });

    var iconField = $('input[name=icon]');
    if (iconField.length)
    {
        $(iconField).unbind('change');
        $(iconField).on('change', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).next().find('span').attr('class', $(this).val());
        });
    }
    var selectIcon = $('.select-icon>.icons-item');
    if (selectIcon.length)
    {
        $(selectIcon).unbind('click');
        $(selectIcon).on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $class = $(this).find('span').attr('class');
            $('input[name=icon]').val($class);
            var selectIconColor = $('.select-icon-color>.icons-item')
            if (selectIconColor.length)
            {
                for (var i = 0; i < selectIconColor.length; i++)
                {
                    $(selectIconColor[i]).html('<span class="' + $class + ' ' + $(selectIconColor[i]).data('color') + '"></span>');
                }
            }
            $('.select-icon').addClass('hidden');
            $('.select-icon-color').removeClass('hidden');
            $('#switchToSelectIcon').removeClass('hidden');
        });
    }
    var selectIconColor = $('.select-icon-color>.icons-item');
    if (selectIconColor.length)
    {
        $(selectIconColor).unbind('click');
        $(selectIconColor).on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $class = $('input[name=icon]').val();
            var selectIconColor2 = $('.select-icon-color>.icons-item')
            if (selectIconColor2.length)
            {
                for (var i = 0; i < selectIconColor2.length; i++)
                {
                    $class = $class.replace(new RegExp($(selectIconColor2[i]).data('color'), "ig"), '');
                }
            }
            $('input[name=icon]').val($class + ' ' + $(this).data('color'));
            $('input[name=icon]').change();

            $('#selectIconClose').click();
        }
        );
    }
    var iconDialog = $('#iconDialog');
    if (iconDialog.length)
    {
        $(iconDialog).unbind('hidden.bs.modal');
        $(iconDialog).on('hidden.bs.modal', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $('.select-icon').removeClass('hidden');
            $('.select-icon-color').addClass('hidden');
            $('#switchToSelectIcon').addClass('hidden');
        });
    }
    var switchToSelectIcon = $('#switchToSelectIcon');
    if (switchToSelectIcon.length)
    {
        $(switchToSelectIcon).unbind('click');
        $(switchToSelectIcon).on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $('.select-icon').removeClass('hidden');
            $('.select-icon-color').addClass('hidden');
            $(switchToSelectIcon).addClass('hidden');
        });
    }
    var dateShower = $('.dateShower');
    if (dateShower.length)
    {
        for (var i = 0; i < dateShower.length; i++)
        {
            var $value = parseInt($(dateShower[i]).data('value'));
            if ($value > 0)
            {
                $(dateShower[i]).html(moment($value * 1000).fromNow() + ' - ' + $(dateShower[i]).html())
            }
        }
    }
    var taskChecks = $('input[type=checkbox]');
    if (taskChecks.length)
    {
        for (var i = 0; i < taskChecks.length; i++)
        {
            $(taskChecks[i]).prettyCheckable();
            $(taskChecks[i]).unbind('change');
            $(taskChecks[i]).on('change', function(e) {
                e.preventDefault();
                e.stopPropagation();
                if ($(this).data('href'))
                {
                    var $data = {};
                    if (this.checked == true)
                    {
                        $data[$(this).data('name')] = 1;
                    }
                    else
                    {
                        $data[$(this).data('name')] = 0;
                    }
                    $.ajax({
                        type: "POST",
                        url: $(this).data('href'),
                        data: $data,
                        error: function(responseText, statusText, xhr, $form) {
                            alert('Нет доступа');
                            return false;
                        }
                    });
                    return false;
                }
            });
        }
    }
    $('.prettycheckbox').find('label').removeAttr('for').unbind('click');

    $("button[type=submit]").click(function() {
        $("button[type=submit]").removeAttr("clicked");
        $(this).attr("clicked", "true");
    });
    var projectForms = $('.item-form');
    if (projectForms.length)
    {
        for (var i = 0; i < projectForms.length; i++)
        {
            $(projectForms[i]).unbind('submit');
            $(projectForms[i]).on('submit', function(e) {
                var localtime = $('input[name=localtime]');
                if (localtime.length)
                {
                    localtime.val(Math.round($.now() / 1000));
                }
                e.preventDefault();
                e.stopPropagation();
                var act = $('input[name=act]');
                if (act.length)
                    act.val($("button[type=submit][clicked=true]").data('act'));
                var form = $(this);
                form.ajaxSubmit({
                    success: function(responseText, statusText, xhr, $form) {
                        if (act.length && act.val() == 'create_and_reset')
                        {
                            form.resetForm();
                        } else
                        {
                            if ($($form.context).attr('id') == 'contactForm')
                                alert('Письмо отправлено!');
                            if ($($form.context).attr('id') == 'projectUpdateForm' && act.val() == 'copy')
                            {
                                if (redirectOn)
                                    window.location = '/project/' + responseText.project_id;
                            }
                            else
                            if ($($form.context).attr('id') == 'projectCreateForm')
                            {
                                if (redirectOn)
                                    window.location = '/projects';
                            }
                            else
                            {
                                if (redirectOn)
                                    window.location = $('input[name=referer]').val();
                            }
                        }
                        return false;
                    },
                    error: function(responseText, statusText, xhr, $form) {
                        return false;
                    }
                });
                return false;
            });
        }
    }
    var authForms = $('#regForm,#loginForm,#activateRegKeyForm,#recoveryForm,#profileForm');
    if (authForms.length)
    {
        for (var i = 0; i < authForms.length; i++)
        {
            $(authForms[i]).unbind('submit');
            $(authForms[i]).on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var localtime = $('input[name=localtime]');
                if (localtime.length)
                {
                    localtime.val(Math.round($.now() / 1000));
                }
                $(this).ajaxSubmit({
                    success: function(responseText, statusText, xhr, $form) {
                        if ($($form.context).attr('id') != 'regForm' && $($form.context).attr('id') != 'recoveryForm') {
                            if (redirectOn)
                                window.location = '/projects';
                        } else
                        {
                            if ($($form.context).attr('id') == 'regForm')
                                alert('На указанный email отправленна ссылка для активации вашей учетной записи');
                            if ($($form.context).attr('id') == 'recoveryForm')
                                alert('На указанный email отправленна ссылка для восстановления доступа');
                            if (redirectOn)
                                window.location = '/';
                        }
                        return false;
                    },
                    error: function(responseText, statusText, xhr, $form) {
                        if ($($form.context).attr('id') == 'recoveryForm')
                        {
                            alert('Ошибка востановления доступа');
                        }
                        else
                        if ($($form.context).attr('id') == 'loginForm')
                        {
                            if (responseText.status == '403')
                            {
                                alert('Ваша учетная запись ещё не активированна, для активации перейдите по ссылке отправленной на email указанный при регистрации');
                            } else {
                                alert('Ошибка авторизации');
                            }
                        }
                        else
                        if ($($form.context).attr('id') == 'regForm')
                        {
                            if (responseText.status == '409')
                            {
                                alert('Пользователь с таким Email уже зарегистрирован!');
                            }
                        }
                        else
                            alert('Ошибка');
                        return false;
                    }
                });
                return false;
            });
        }
    }
    var logoutLink = $('.logoutLink');
    if (logoutLink.length)
    {
        $(logoutLink).unbind('click');
        logoutLink.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $.ajax({
                type: "POST",
                url: '/logout',
                success: function() {
                    if (redirectOn)
                        window.location = '/';
                    return false;
                }
            });
            return false;
        });
    }

    var deleteItemLink = $('.deleteItemLink');
    if (deleteItemLink.length)
    {
        $(deleteItemLink).unbind('click');
        deleteItemLink.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var $href = $(this).data('href');
            var $redirect = $(this).data('redirect');
            $.ajax({
                type: "POST",
                url: $href,
                success: function() {
                    if ($redirect == null)
                    {
                        if (redirectOn)
                            window.location = window.location;
                    }
                    else
                    {
                        if (redirectOn)
                            window.location = $redirect;
                    }
                    return false;
                },
                error: function(responseText, statusText, xhr, $form) {
                    alert('Нет доступа');
                    return false;
                }
            });
            return false;
        });
    }
    var deleteProjectLink = $('.deleteProjectLink');
    if (deleteProjectLink.length)
    {
        $(deleteProjectLink).unbind('click');
        deleteProjectLink.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $.ajax({
                type: "POST",
                url: $(this).data('href'),
                success: function() {
                    if (redirectOn)
                        window.location = '/projects';
                    return false;
                },
                error: function(responseText, statusText, xhr, $form) {
                    alert('Нет доступа');
                    return false;
                }
            });
            return false;
        });
    }
    var toProjectLink = $('.toProjectLink');
    if (toProjectLink.length)
    {
        $(toProjectLink).unbind('click');
        toProjectLink.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (redirectOn)
                window.location = $(this).data('href')
            return false;
        });
    }
    var datetotimestamp = $('.datetotimestamp');
    if (datetotimestamp.length)
    {
        for (var i = 0; i < datetotimestamp.length; i++)
        {
            var $target = $($(datetotimestamp[i]).data('target'));
            var $timestamp = parseInt($target.val());
            if ($timestamp > 0)
                $(datetotimestamp[i]).val(moment($timestamp * 1000).format('DD.MM.YYYY HH:mm'));
            else
                $(datetotimestamp[i]).val('');
            $(datetotimestamp[i]).unbind('change');
            $(datetotimestamp[i]).on('change', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var $target = $($(this).data('target'));
                if (parseInt($(this).val()) > 0)
                    $target.val(Math.round(moment($(this).val(), 'DD.MM.YYYY HH:mm').unix()));
                else
                    $target.val(0);

                return false;
            });

        }
    }
    var datetimeFields = $('.datetime-field');
    if (datetimeFields.length)
    {
        for (var i = 0; i < datetimeFields.length; i++)
        {
            $(datetimeFields[i]).datetimepicker({format: 'DD.MM.YYYY HH:mm', language: 'ru'});
            $(datetimeFields[i]).unbind('dp.change');
            $(datetimeFields[i]).on('dp.change', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var $target = $($(this).find('input').data('target'));
                if (parseInt($(this).find('input').val()) > 0)
                    $target.val(Math.round(moment($(this).find('input').val(), 'DD.MM.YYYY HH:mm').unix()));
                else
                    $target.val(0);
                return false;
            });
        }
    }

    var items = $('.tasks-item');
    if (items.length)
    {
        if (items.length > 10)
            $('#tasks-list').find('.pagination,.search').removeClass('hidden');
        else
            $('#tasks-list').find('.pagination,.search').addClass('hidden');
        var tasksList = new List('tasks-list', {
            valueNames: ['ls-title'],
            page: 10,
            plugins: [ListPagination({})]
        });
        tasksList.on('searchComplete', function() {
            if (tasksList.visibleItems.length > 10)
                $('#tasks-list').find('.pagination').removeClass('hidden');
            else
                $('#tasks-list').find('.pagination').addClass('hidden');
        });
    }
    var items = $('.projects-item');
    if (items.length)
    {
        if (items.length > 10)
            $('#projects-list').find('.pagination,.search').removeClass('hidden');
        else
            $('#projects-list').find('.pagination,.search').addClass('hidden');
        var projectsList = new List('projects-list', {
            valueNames: ['ls-title','ls-description'],
            page: 10,
            plugins: [ListPagination({})]
        });
        projectsList.on('searchComplete', function() {
            if (projectsList.visibleItems.length > 10)
                $('#projects-list').find('.pagination').removeClass('hidden');
            else
                $('#projects-list').find('.pagination').addClass('hidden');
        });
    }
});