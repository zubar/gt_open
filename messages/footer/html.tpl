<p>Если у Вас возникли дополнительные вопросы, пожалуйста, пишите нам на <a href=mailto:{{@site.support.email}}><b>{{@site.support.email}}</b></a></p>

<p>С наилучшими пожеланиями,</p>
<p><b>{{@site.company}}</b>!</p>